<?php

/** @var $step */
assert($step instanceof Components\Step);

echo isset($_SESSION['step']) ? 'Step of session: ' . unserialize($_SESSION['step']) : '';
?>
<div class="container">
  <div class="row">
    <div class="col-md-12 center content-center">
      <h3>Step <?= $step->getName() ?></h3>
      <p><?= $step->getDescription() ?></p>

      <div class="row">
          <?php $count = count($step->getNextSteps()); ?>
          <?php if ($count > 0): ?>
              <?php foreach ($step->getNextSteps() as $nextStep): ?>
              <div class="col-md-<?= $count > 1 ? 12/$count : 12 ?>">
                <form action="/step" method="post">
                  <input type="number" name="id" value="<?= $nextStep['next_step'] ?>" hidden>
                  <button>Go next step</button>
                </form>
              </div>
              <?php endforeach; ?>
          <?php endif; ?>

          <?php if ($step->getGameOver()): ?>
            <div class="col-md-12">
              <p>GAME OVER BRO!</p>
                <?php if ($step->getTrap()): ?>
                  <div class="col-md-12">
                    <p>Oh nooo, you got trapped. Try again</p>
                  </div>
                <?php endif; ?>
              <div class="col-md-12">
                <form action="/step" method="get">
                  <input type="number" name="id" value="1" hidden>
                  <button>Restart</button>
                </form>
              </div>
            </div>
          <?php endif; ?>
      </div>
    </div>
  </div>
</div>