<?php


namespace Models;


/**
 * Class StepNextModel
 *
 * @package Models
 */
class StepNextModel extends BaseModel
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'step_next';
    }

    /**
     * @param $parentId
     *
     * @return array
     */
    public static function findByParent($parentId)
    {
        $query = self::getDb()->query('SELECT * FROM ' . static::tableName() . ' WHERE parent_id = ' . $parentId);

        return static::fetchAll($query);
    }
}