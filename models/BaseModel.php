<?php

namespace Models;


use App\Db;


/**
 * Class BaseModel
 *
 * @package Models
 */
class BaseModel
{
    /**
     * @return \PDO
     */
    public static function getDb()
    {
        return Db::getInstance();
    }

    /**
     * @param $query
     *
     * @return array
     */
    public static function fetchOne($query)
    {
        return static::asArray($query->fetch(\PDO::FETCH_ASSOC));
    }

    /**
     * @param $query
     *
     * @return array
     */
    public static function fetchAll($query)
    {
        return static::asArray($query->fetchAll(\PDO::FETCH_ASSOC));
    }

    /**
     * @param array $query
     *
     * @return array
     */
    private static function asArray($query)
    {
        if (!is_array($query)) {
            throw new \PDOException('Not found');
        }

        $fields = [];
        foreach ($query as $filed => $value) {
            $fields[$filed] = $value;
        }

        return $fields;
    }
}