<?php


namespace Models;


/**
 * Class StepModel
 *
 * @package Models
 */
class StepModel extends BaseModel
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'step';
    }

    /**
     * @param $step
     *
     * @return array
     */
    public static function findByStep($step)
    {
        $query = self::getDb()->query('SELECT * FROM ' . static::tableName() . ' WHERE step = ' . $step);

        return static::fetchOne($query);
    }
}