<?php

require __DIR__ . '/config/db.php';

try {
    $conn = new PDO('mysql:dbname='.$db['name'].';'.'host='.$db['host'], $db['user'], $db['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $step = "CREATE TABLE IF NOT EXISTS step (
        id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(255) NOT NULL,
        description TEXT NOT NULL,
        step INT(11) NOT NULL,
        trap INT(11) NOT NULL DEFAULT '0',
        game_over TINYINT(1) NOT NULL DEFAULT '0'
    )";

    $conn->exec($step);
    echo "Table step created successfully" . PHP_EOL;

    $delete = "DELETE FROM step";
    $conn->exec($delete);

    $insertStep .= "INSERT INTO step (`id`, `name`, `description`, `step`, `game_over`, `trap`) VALUES
        (1, 'One', 'Hi in a game. It`s your fist step. Good luck bro ;)', 1, 0, 0),
        (2, 'Two', 'Wow, i`m surprised. Go go go!!!', 2, 0, 0),
        (3, 'Three', 'Nice. Your went a half of the game. Be careful traps can expect you', 3, 0, 0),
        (4, 'Four', 'It`s just amazing how far you come.', 4, 0, 0),
        (5, 'Five', 'Congratulations. You completed the game', 5, 1, 0),
        (6, 'Six', 'You were defeated', 6, 1, 1);
    ";

    $conn->exec($insertStep);
    echo "Inserted to step successfully" . PHP_EOL;

    $stepNext = "CREATE TABLE IF NOT EXISTS step_next (
        id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        parent_id INT(11) NOT NULL,
        next_step INT(11) NOT NULL
    )";

    $conn->exec($stepNext);
    echo "Table step_next created successfully" . PHP_EOL;

    $delete = "DELETE FROM step_next";
    $conn->exec($delete);

    $insertStepNext .= "INSERT INTO step_next (`id`, `parent_id`, `next_step`) VALUES
        (1, 1, 4),
        (2, 1, 2),
        (3, 2, 3),
        (4, 4, 5),
        (5, 3, 6);
    ";

    $conn->exec($insertStepNext);
    echo "Inserted to step_next successfully" . PHP_EOL;
}
catch(PDOException $e)
{
    echo 'Connection failed: ' . $e->getMessage();
}
$conn = null;