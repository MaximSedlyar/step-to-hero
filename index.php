<?php

chdir(dirname(__DIR__));
require 'vendor/autoload.php';

require __DIR__ . '/config/routes.php';
require __DIR__ . '/config/db.php';

$config = new \App\Container();
$config->set('routes', $routes);
$config->set('db', $db);

$app = new \App\App($config);