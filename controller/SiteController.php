<?php


namespace Controller;


/**
 * Class SiteController
 *
 * @package Controller
 */
class SiteController extends BaseController
{
    public function actionIndex()
    {
        $this->view->render('index');
    }
}