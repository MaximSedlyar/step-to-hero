<?php


namespace Controller;


use App\Router;
use App\View;


/**
 * Class BaseController
 *
 * @package Controller
 */
class BaseController
{
    const DEFAULT_STEP = 1;

    /** @var int|mixed  */
    private $step;

    /** @var View  */
    public $view;

    /** @var Router  */
    private $router;

    /**
     * BaseController constructor.
     *
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
        $this->view = new View();
        $this->step = unserialize($_SESSION['step']);
    }

    public function setStep($step): void
    {
        if ($this->getRouter()->isGet()) {
            $_SESSION['step'] = serialize(self::DEFAULT_STEP);
            $this->step = self::DEFAULT_STEP;
        }

        if ($this->getRouter()->isPost()) {
            $_SESSION['step'] = serialize($step);
            $this->step = $step;
        }
    }

    /**
     * @return int|mixed
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @return Router
     */
    public function getRouter()
    {
        return $this->router;
    }
}