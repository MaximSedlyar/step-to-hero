<?php


namespace Controller;


use Components\StepMapper;
use Components\Storage;
use Models\StepModel;
use Models\StepNextModel;

/**
 * Class StepController
 *
 * @package Controller
 */
class StepController extends BaseController
{
    /**
     * @param $step
     */
    public function actionStep($step)
    {
        $this->setStep((int)$step);
        $model = StepModel::findByStep($this->getStep());
        $model['next_steps'] = StepNextModel::findByParent($model['id']);

        $storage = new Storage($model);
        $mapper = new StepMapper($storage);
        $stetObject = $mapper->getStep();

        $this->view->render('step', ['step' => $stetObject]);
    }
}