<?php


namespace Components;


/**
 * Class Step
 *
 * @package Components
 */
class Step
{
    /** @var string */
    private $name;

    /** @var string */
    private $description;

    /** @var int */
    private $step;

    /** @var int */
    private $game_over;

    /** @var int */
    private $trap;

    /** @var array */
    private $next_steps = [];

    /**
     * Step constructor.
     *
     * @param $name
     * @param $description
     * @param $step
     * @param $game_over
     * @param $trap
     * @param $next_steps
     */
    public function __construct($name, $description, $step, $game_over, $trap, $next_steps)
    {
        $this->name = $name;
        $this->description = $description;
        $this->step = $step;
        $this->game_over = $game_over;
        $this->trap = $trap;
        $this->next_steps = $next_steps;
    }

    /**
     * @param array $state
     *
     * @return Step
     */
    public static function fromState(array $state): Step
    {
        return new self(
            $state['name'],
            $state['description'],
            $state['step'],
            $state['game_over'],
            $state['trap'],
            $state['next_steps']
        );
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getGameOver(): int
    {
        return $this->game_over;
    }

    /**
     * @return int
     */
    public function getTrap(): int
    {
        return $this->trap;
    }

    /**
     * @return mixed
     */
    public function getNextSteps()
    {
        return $this->next_steps;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getStep()
    {
        return $this->step;
    }
}