<?php


namespace Components;


class StepMapper
{
    /** @var Storage  */
    private $storage;

    /**
     * StepMapper constructor.
     *
     * @param Storage $storage
     */
    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @return Step
     */
    public function getStep(): Step
    {
        $result = $this->storage->getData();

        return $this->mapRowToUser($result);
    }

    /**
     * @param array $row
     *
     * @return Step
     */
    private function mapRowToUser(array $row): Step
    {
        return Step::fromState($row);
    }
}