<?php


namespace Components;


/**
 * Class Storage
 *
 * @package Components
 */
class Storage
{
    /** @var array  */
    private $data = [];

    /**
     * Storage constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}