<?php


namespace App;


use Laminas\Diactoros\ServerRequestFactory;


/**
 * Class Router
 *
 * @package app
 */
class Router
{
    /** @var string */
    private $defaultController = '\Controller\SiteController';

    /** @var string */
    private $defaultAction = 'actionIndex';

    /** @var array */
    private $routes = [];

    /** @var array */
    private $params = [];

    /** @var ServerRequestFactory */
    private $request;

    /**
     * Router constructor.
     *
     * @param array $routes
     */
    public function __construct(array $routes)
    {
        $this->request = ServerRequestFactory::fromGlobals();
        $this->routes = $routes;
    }

    public function run()
    {
        $uri = $this->getUri();

        $result = null;
        foreach ($this->routes as $route => $path) {
            if (preg_match("~$route~", str_replace('?', '', $uri))) {

                $internalRoute = preg_replace("~$route~", $path, $uri);

                $segments = explode('/', $internalRoute);

                $controllerName = $this->getControllerName($segments);
                $actionName = $this->getActionName($segments);

                $this->params = $this->getParams();
                if (empty($this->params)) {
                    throw new \ArgumentCountError('Not found params');
                }
                call_user_func_array([new $controllerName($this), $actionName], $this->params);
                $result = true;
            }
        }

        if (!$result) {
            call_user_func_array([new $this->defaultController($this), $this->defaultAction], [$this->request]);
        }
    }

    /**
     * Check if this GET Method
     *
     * @return bool
     */
    public function isGet(): bool
    {
        return $this->request->getMethod() == 'GET';
    }

    /**
     * Check if this POST Method
     *
     * @return bool
     */
    public function isPost(): bool
    {
        return $this->request->getMethod() == 'POST';
    }

    /**
     * Get url path
     *
     * @return string
     */
    private function getUri(): string
    {
        return $this->request->getUri()->getPath();
    }

    /**
     * Return Controller name
     *
     * @param array $segments
     *
     * @return string
     */
    private function getControllerName(array &$segments): string
    {
        return '\Controller\\' . ucfirst(array_shift($segments)) . 'Controller';
    }

    /**
     * Return Action name
     *
     * @param array $segments
     *
     * @return string
     */
    private function getActionName(array &$segments): string
    {
        return 'action' . ucfirst(array_shift($segments));
    }

    /**
     * Return array params
     *
     * @return array
     */
    private function getParams(): array
    {
        if ($this->isGet()) {
            return $this->request->getQueryParams();
        }

        if ($this->isPost()) {
            return $this->request->getParsedBody();
        }
    }
}