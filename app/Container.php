<?php


namespace App;


use http\Exception\InvalidArgumentException;


/**
 * Class Container
 *
 * @package Config
 */
class Container
{
    /** @var array  */
    private $definitions = [];

    /**
     * @param $id
     *
     * @return mixed
     */
    public function get($id)
    {
        if (!array_key_exists($id, $this->definitions)) {
            throw new InvalidArgumentException('Undefined props "' . $id . '"');
        }

        return $this->definitions[$id];
    }

    /**
     * @param $id
     * @param $value
     */
    public function set($id, $value): void
    {
        $this->definitions[$id] = $value;
    }
}