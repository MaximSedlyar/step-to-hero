<?php

namespace App;


/**
 * Class View
 *
 * @package App
 */
class View
{
    /**
     * @param       $template
     * @param array $params
     *
     * @return false|string
     */
    protected function fetchPartial($template, $params = []){
        extract($params);
        ob_start();
        include dirname(dirname(__FILE__)).'/views/'.$template.'.php';
        return ob_get_clean();
    }

    /**
     * @param       $template
     * @param array $params
     *
     * @return false|string
     */
    protected function fetch($template, $params = []){
        $content = $this->fetchPartial($template, $params);
        return $this->fetchPartial('layout', ['content' => $content]);
    }

    /**
     * @param       $template
     * @param array $params
     */
    public function render($template, $params = []){
        echo $this->fetch($template, $params);
    }
}