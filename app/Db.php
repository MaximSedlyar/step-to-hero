<?php


namespace App;


use PDO;


/**
 * Class Db
 *
 * @package App
 */
class Db
{
    /** @var array  */
    private static $config = [];

    /** @var null | PDO */
    private static $connection = null;

    /**
     * @param array $config
     */
    public static function setConfig(array $config)
    {
        self::$config = $config;
    }

    /**
     * @return PDO
     */
    public static function getInstance()
    {
        if (static::$connection == null) {
            static::$connection = static::conn();
        }

        return static::$connection;
    }

    /**
     * @return PDO
     */
    private static function conn()
    {
        return new PDO(
            'mysql:host=' . self::$config['host'] . ';dbname=' . self::$config['name'],
            self::$config['user'],
            self::$config['pass'],
            [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]
        );
    }

    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}
}