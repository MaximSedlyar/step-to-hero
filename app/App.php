<?php


namespace App;


/**
 * Class App
 *
 * @package app
 */
class App
{
    /** @var Router  */
    private $router;

    public function __construct(Container $config)
    {
        Db::setConfig($config->get('db'));

        $this->router = new Router($config->get('routes'));
        $this->router->run();
    }
}